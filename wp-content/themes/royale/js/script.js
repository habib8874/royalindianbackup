$(function () {

    $(document).ready(function () {
        var stickyNavTop = $('#head').offset().top;
        var stickyNav = function () {
            var scrollTop = $(window).scrollTop(); // our current vertical position from the top

            if (scrollTop > 50) {
                $('#head').addClass('sticky');
            } else {
                $('#head').removeClass('sticky');
            }
        };

        stickyNav();
        $(window).scroll(function () {
            stickyNav();
        });
    });

});
$(function () {

    $(document).ready(function(){ 
        $(window).scroll(function(){ 
            if ($(this).scrollTop() > 100) { 
                $('#scroll').fadeIn(); 
            } else { 
                $('#scroll').fadeOut(); 
            } 
        });
    
        $('#scroll').click(function(){ 
            $("html, body").animate({ scrollTop: 0 }, 600); 
            return false; 
        }); 
    });
});