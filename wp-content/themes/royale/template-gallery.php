<?php
/*
Template Name:  Gallery page Template
*/
 get_header(); ?>
 <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<section class="homeGallerySection clearfix">
		<div class="container">
			<span class="smallHeading">Our</span>
			<h2 class="largeHeading">Gallery</h2>
			
			<div class="galleryListBlock clearfix">
			
				<div class="bar clearfix">
				<?php
				  $count1 = 0;
				  $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
				  $args = array( 'post_type' => 'our_gallery', 'posts_per_page' => 18, 'paged' => $paged);
                 $query = new WP_Query( $args ); 
				 if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); $count1++; ?>
					<div class="col-sm-4 col-xs-12 box">
						<div class="imgBox clearfix">
						<a href="<?php the_post_thumbnail_url('full'); ?>" class="thumbnail" title="<?php the_title(); ?>">
							<img src="<?php the_post_thumbnail_url('gallery-thumb-size'); ?>"  alt="<?php the_title(); ?>" class="img-fluid">
							</a>
						</div>
					<!--	<h2><?php // the_title(); ?></h2>
						<?php //the_content(); ?> -->
					</div>
					<?php if($count1 % 3 == 0) {?>
					</div>
					<div class="bar clearfix">
					<?php } ?>
					<?php endwhile; else : ?>
	<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; wp_reset_query(); ?>
				</div>
				<?php 
				$big = 999999999; // need an unlikely integer
 echo paginate_links( array(
    'base' => str_replace( $big, '%#%', get_pagenum_link( $big ) ),
    'format' => '?paged=%#%',
    'current' => max( 1, get_query_var('paged') ),
    'total' => $query->max_num_pages
) );

				?>
				<!---<div class="bar clearfix">
					<div class="col-sm-4 col-xs-12 box">
						<div class="imgBox clearfix">
							<img src="<?php bloginfo('template_directory'); ?>/images/gallery_Img1.jpg" alt="Gallery Img1">
						</div>
					</div>
					<div class="col-sm-4 col-xs-12 box">
						<div class="imgBox clearfix">
							<img src="<?php bloginfo('template_directory'); ?>/images/gallery_Img2.jpg" alt="Gallery Img2">
						</div>
					</div>
					<div class="col-sm-4 col-xs-12 box">
						<div class="imgBox clearfix">
							<img src="<?php bloginfo('template_directory'); ?>/images/gallery_Img3.jpg" alt="Gallery Img3">
						</div>
					</div>
				</div>
				<div class="bar clearfix">
					<div class="col-sm-4 col-xs-12 box">
						<div class="imgBox clearfix">
							<img src="<?php bloginfo('template_directory'); ?>/images/gallery_Img4.jpg" alt="Gallery Img4">
						</div>
					</div>
					<div class="col-sm-4 col-xs-12 box">
						<div class="imgBox clearfix">
							<img src="<?php bloginfo('template_directory'); ?>/images/gallery_Img5.jpg" alt="Gallery Img5">
						</div>
					</div>
					<div class="col-sm-4 col-xs-12 box">
						<div class="imgBox clearfix">
							<img src="<?php bloginfo('template_directory'); ?>/images/gallery_Img6.jpg" alt="Gallery Img6">
						</div>
					</div>
				</div>-->
				
			</div>
			
		</div>
	</section>
	<?php endwhile; else : ?>
	<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; wp_reset_query(); ?>
<?php get_footer(); ?>