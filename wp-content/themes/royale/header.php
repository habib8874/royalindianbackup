<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="profile" href="https://gmpg.org/xfn/11" />
	<?php wp_head(); ?>
	<link rel="shortcut icon" type="image/x-icon" href="<?php bloginfo('template_directory'); ?>/images/favicon.ico">
	<link href="<?php bloginfo('template_directory'); ?>/css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="<?php bloginfo('template_directory'); ?>/css/font.css" rel="stylesheet" type="text/css">
	<link href="<?php bloginfo('template_directory'); ?>/css/viewbox.css" rel="stylesheet" type="text/css">
	<link href="<?php bloginfo('template_directory'); ?>/css/style.css?v0056" rel="stylesheet" type="text/css">
</head>

<body  id="page-top" data-spy="scroll" data-target=".navbar-fixed-top" <?php body_class(); ?>>
<header class="clearfix" id="head">
		
		<div class="head clearfix">
			<div class="contactBox clearfix">
			<i class="icon icon-smartphone-call"></i> 
			<!--<div class="info">
				<span>+64 7-856 7716</span>
				<small>08:00 am - 10:30 pm</small>
			</div>-->
			<?php dynamic_sidebar('sidebar-2'); ?>
		</div>
		
			<nav class="navbar navbar-inverse">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">  
				<button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>  
		<a href="<?php bloginfo('url'); ?>" class="navbar-brand logo page-scroll"><?php //dynamic_sidebar('header-logo'); ?> <img src="<?php bloginfo('template_directory'); ?>//images/logo.png" /></a>
			</div> 
			<style>
				header .navbar .navbar-header .navbar-brand img{
					display: inline;
				}
				.main-menu-more,h1:not(.site-title):before, h2:before{
					display:none;
				}
				
			</style>
			<!-- Collection of nav links, forms, and other content for toggling -->
			<!---<div id="navbarCollapse" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li><a href="#">Home</a></li>
					<li><a href="#">About Us</a></li>
					<li><a href="#">Order Online</a></li>
					<li><a href="#">Gallery</a></li>
					<li><a href="#">Contact Us</a></li>
					<li class="loginLink"><a href="#">Login/Register</a></li>
				</ul>
			</div>--->
			<?php wp_nav_menu( array( 'container' => 'div', 'container_class' => 'collapse navbar-collapse', 'container_id' => 'navbarCollapse', 'menu_class' => 'nav navbar-nav','theme_location' => 'menu-1' )); ?>
		</nav>
		<div class="loginOption clearfix">
			<a href="https://royaleindianbranch.booknorder.co.nz/" class="orderOnlineBtn" target="_blank">Order Online</a>
		</div>
		<?php if ( !is_user_logged_in() ) { ?>
			<div class="loginOption clearfix">
			<!--<a href="<?php // bloginfo('url'); ?>/wp-admin" target="_blank">Login/Register</a> -->
		</div>
		<?php } 
		else { ?>
			<div class="loginOption clearfix">
			<!--<a href="<?php // echo wp_logout_url( home_url() ); ?>">Logout</a>-->
		</div>
			
		<?php } ?>
		</div>
		<div class="curveLine clearfix">
			<div class="line"></div>
		</div>		
	</header>
