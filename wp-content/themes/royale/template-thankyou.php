<?php
/*
Template Name:  thank you page Template
*/
 get_header(); ?>
 <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<section class="homeGallerySection clearfix">
		<div class="container">
			<div class="thanksYouPage">
			<div class="text-xs-center">
			<h1 class="display-3">Thank You!</h1>
			<p class="lead"><strong>Please check your email</strong></p>			
			<p class="lead"><a class="btn btn-primary btn-sm" href="/" role="button">Continue to homepage</a></p>
			</div>
	</div>
		</div>
	</section>
	
	<section class="testiSection clearfix">
		<div class="mask"></div>
		
		<div class="container">
			
			<span class="smallHeading">What</span>
			<h2 class="largeHeading">People Say About us</h2>
			
        <div class="testiBlock clearfix">
			
			<div class="carousel slide" data-ride="carousel" id="quote-carousel">
				<!-- Carousel Buttons Next/Prev -->
				<div class="navigation clearfix">
					 <a data-slide="prev" href="#quote-carousel" class="left carousel-control"><i class="icon icon-arrow-left"></i></a>
                    <a data-slide="next" href="#quote-carousel" class="right carousel-control"><i class="icon icon-arrow-right"></i></a>
				</div>
                   
                    <!-- Carousel Slides / Quotes -->
                    <div class="carousel-inner text-center">
                        <!-- Quote 1 -->
						<?php
				  $count2 = 0;
				  $args = array( 'post_type'   => 'our_testimonials', 'posts_per_page' => 10);
                 $query = new WP_Query( $args ); 
				 if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); $count2++; ?>
                        <div class="item <?php if($count2 == 1) {?>active<?php }?>">
                            <div class="testi clearfix">
							<?php the_content(); ?>
							<span><?php the_title(); ?></span>
						</div>
                        </div>
						<?php endwhile; else : ?>
	<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; wp_reset_query(); ?>
						 
                    </div>
                </div>			
        </div>
			
    	</div>
		
	</section>
	<section class="homeGallerySection clearfix">
		<div class="container">
			<span class="smallHeading">Our</span>
			<h2 class="largeHeading">Gallery</h2>
			
			<div class="galleryListBlock clearfix">
			
				<div class="bar clearfix">
				<?php
				  $count1 = 0;
				  $args2 = array( 'post_type'   => 'our_gallery');
				  $args1 = array( 'post_type'   => 'our_gallery', 'posts_per_page' => 6);
                 $query1 = new WP_Query( $args1 );
                  $query2 = new WP_Query( $args2 );				 
				 if ( $query1->have_posts() ) : while ( $query1->have_posts() ) : $query1->the_post(); $count1++; ?>
				 
					<div class="col-sm-4 col-xs-12 box">
						<div class="imgBox clearfix">
						<a href="<?php the_post_thumbnail_url('full'); ?>" class="thumbnail" title="<?php the_title(); ?>">
							<img src="<?php the_post_thumbnail_url('gallery-thumb-size'); ?>" alt="<?php the_title(); ?>" class="img-fluid">
						</a>
						</div>
						<!--<h2><?php //the_title(); ?></h2>
						<?php //the_content(); ?> -->
					</div>
					<?php if($count1 == 3) {?>
					</div>
					<div class="bar clearfix">
					<?php } ?>
					<?php endwhile; else : ?>
	<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; wp_reset_query(); ?>
				</div>
				<?php $post_count = $query2->post_count;
                if($post_count>6) {?>
				<h2 class="text-center"><a href="/gallery/">View All</a></h2>	
					
				<?php } ?>
			</div>
		</div>
	</section>
	<?php endwhile; else : ?>
	<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; wp_reset_query(); ?>
<?php get_footer(); ?>