<?php
/*
Template Name:  Home page Template
*/
 get_header(); ?>
 <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<section class="banner clearfix">
		<div id="banner" class="bannerSection clearfix" style="display:none;">
			<div id="myCarousel" class="carousel slide">
				<!-- Indicators -->
				<ol class="carousel-indicators">
					<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
					<li data-target="#myCarousel" data-slide-to="1"></li>
					<li data-target="#myCarousel" data-slide-to="2"></li>
					<li data-target="#myCarousel" data-slide-to="3"></li>
					<li data-target="#myCarousel" data-slide-to="4"></li>
				</ol>

				<!-- Wrapper for Slides -->
				<div class="carousel-inner">
                  <?php
				  $count = 0;
				  $args = array( 'post_type'   => 'post_type', 'posts_per_page' => 10);
                 $query = new WP_Query( $args ); 
				 if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); $count++; ?>
				 
					<div class="item <?php if($count == 1) {?>active<?php } ?>">
						<div class="mask"></div>
						<div class="fill" style="background-image: url(<?php the_post_thumbnail_url('full'); ?>)"></div>
					</div>
					<?php endwhile; endif; wp_reset_query(); ?>
					
				</div>

				<!-- Controls -->
				<a class="left carousel-control" href="#myCarousel" data-slide="prev">
					<div class="icon">
						<span class="icon-prev"></span>
					</div>
				</a>
				<a class="right carousel-control" href="#myCarousel" data-slide="next">
					<div class="icon">
						<span class="icon-next"></span>
					</div>
				</a>
			</div>
		</div>
		<div id="banner" class="bannerSection clearfix">
			<div id="myCarousel" class="carousel slide">
				<!-- Indicators -->
				<ol class="carousel-indicators">
					<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
					<li data-target="#myCarousel" data-slide-to="1"></li>
					<li data-target="#myCarousel" data-slide-to="2"></li>
					
					<li data-target="#myCarousel" data-slide-to="3"></li>
					<li data-target="#myCarousel" data-slide-to="4"></li>
					<li data-target="#myCarousel" data-slide-to="5"></li>
					<li data-target="#myCarousel" data-slide-to="6"></li>
					<li data-target="#myCarousel" data-slide-to="7"></li>
					
				</ol>

				<!-- Wrapper for Slides -->
				<div class="carousel-inner">

					<div class="item active">
						<div class="mask"></div>
						<div class="fill" style="background-image: url(<?php bloginfo('template_directory'); ?>/images/banner1.jpg)"></div>
					</div>
					<div class="item">
						<div class="mask"></div>
						<div class="fill" style="background-image: url(<?php bloginfo('template_directory'); ?>/images/banner2.jpg)"></div>
					</div>
					<div class="item">
						<div class="mask"></div>
						<div class="fill" style="background-image: url(<?php bloginfo('template_directory'); ?>/images/banner3.jpg)"></div>
					</div>
					<div class="item">
						<div class="mask"></div>
						<div class="fill" style="background-image: url(<?php bloginfo('template_directory'); ?>/images/banner119.jpg)"></div>
					</div>
					<div class="item">
						<div class="mask"></div>
						<div class="fill" style="background-image: url(<?php bloginfo('template_directory'); ?>/images/banner219.jpg)"></div>
					</div>
					<div class="item">
						<div class="mask"></div>
						<div class="fill" style="background-image: url(<?php bloginfo('template_directory'); ?>/images/banner319.jpg)"></div>
					</div>
					<div class="item">
						<div class="mask"></div>
						<div class="fill" style="background-image: url(<?php bloginfo('template_directory'); ?>/images/banner419.jpg)"></div>
					</div>
					<div class="item">
						<div class="mask"></div>
						<div class="fill" style="background-image: url(<?php bloginfo('template_directory'); ?>/images/banner519.jpg)"></div>
					</div>
					
				</div>

				<!-- Controls -->
				<a class="left carousel-control" href="#myCarousel" data-slide="prev">
					<div class="icon">
						<span class="icon-prev"></span>
					</div>
				</a>
				<a class="right carousel-control" href="#myCarousel" data-slide="next">
					<div class="icon">
						<span class="icon-next"></span>
					</div>
				</a>
			</div>
		</div>
	</section>
	<div class="clear"></div>
	<section class="homeAboutSection clearfix">
		<div class="container">
			<?php the_content(); ?>
		</div>
	</section>
	
	<section class="testiSection clearfix">
		<div class="mask"></div>
		<!--<div class="container">
			
			
			<div>
				<div class="mask"></div>
				
					<i class="icon icon-arrow-left"></i>
					<i class="icon icon-arrow-right"></i>
				</div>
				<div class="testi clearfix">
				
					<p>"It was a fantastic evening in your superb restaurant which was enjoyed by everyone who was fortunate to be there" </p>
					
					<span>Julie Deowater</span>
					
				</div>
			</div>			
		</div>-->
		
		<div class="container">
			
			<span class="smallHeading">What</span>
			<h2 class="largeHeading">People Say About us</h2>
			
        <div class="testiBlock clearfix">
			
			<div class="carousel slide" data-ride="carousel" id="quote-carousel">
				<!-- Carousel Buttons Next/Prev -->
				<div class="navigation clearfix">
					 <a data-slide="prev" href="#quote-carousel" class="left carousel-control"><i class="icon icon-arrow-left"></i></a>
                    <a data-slide="next" href="#quote-carousel" class="right carousel-control"><i class="icon icon-arrow-right"></i></a>
				</div>
                   
                    <!-- Carousel Slides / Quotes -->
                    <div class="carousel-inner text-center">
                        <!-- Quote 1 -->
						<?php
				  $count2 = 0;
				  $args = array( 'post_type'   => 'our_testimonials', 'posts_per_page' => 10);
                 $query = new WP_Query( $args ); 
				 if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); $count2++; ?>
                        <div class="item <?php if($count2 == 1) {?>active<?php }?>">
                            <div class="testi clearfix">
							<?php the_content(); ?>
							<span><?php the_title(); ?></span>
						</div>
                        </div>
						<?php endwhile; else : ?>
	<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; wp_reset_query(); ?>
						 <!---<div class="item active">
                            <div class="testi clearfix">
							<p>"It was a fantastic evening in your superb restaurant which was enjoyed by everyone who was fortunate to be there" </p>
							<span>Julie Deowater</span>
						</div>
                        </div>
                        <div class="item">
                            <div class="testi clearfix">
							<p>"It was a fantastic evening in your superb restaurant which was enjoyed by everyone who was fortunate to be there" </p>
							<span>John Deowater</span>
						</div>
                        </div>
                        <div class="item">
                            <div class="testi clearfix">
							<p>"It was a fantastic evening in your superb restaurant which was enjoyed by everyone who was fortunate to be there" </p>
							<span>Smith Deowater</span>
					
						</div>
                        </div>--->
                    </div>

                    
                </div>
			
        </div>
			
    	</div>
		
	</section>
	
	<section class="homeGallerySection clearfix">
		<div class="container">
			<span class="smallHeading">Our</span>
			<h2 class="largeHeading">Gallery</h2>
			
			<div class="galleryListBlock clearfix">
			
				<div class="bar clearfix">
				<?php
				  $count1 = 0;
				  $args2 = array( 'post_type'   => 'our_gallery');
				  $args1 = array( 'post_type'   => 'our_gallery', 'posts_per_page' => 6);
                 $query1 = new WP_Query( $args1 );
                  $query2 = new WP_Query( $args2 );				 
				 if ( $query1->have_posts() ) : while ( $query1->have_posts() ) : $query1->the_post(); $count1++; ?>
				 
					<div class="col-sm-4 col-xs-12 box">
						<div class="imgBox clearfix">
						<a href="<?php the_post_thumbnail_url('full'); ?>" class="thumbnail" title="<?php the_title(); ?>">
							<img src="<?php the_post_thumbnail_url('gallery-thumb-size'); ?>" alt="<?php the_title(); ?>" class="img-fluid">
						</a>
						</div>
						<!--<h2><?php //the_title(); ?></h2>
						<?php //the_content(); ?> -->
					</div>
					<?php if($count1 == 3) {?>
					</div>
					<div class="bar clearfix">
					<?php } ?>
					<?php endwhile; else : ?>
	<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; wp_reset_query(); ?>
				</div>
				<?php $post_count = $query2->post_count;
                if($post_count>6) {?>
				<h2 class="text-center"><a href="/gallery/">View All</a></h2>	
					
				<?php } ?>
				
				


				<!---<div class="bar clearfix">
					<div class="col-sm-4 col-xs-12 box">
						<div class="imgBox clearfix">
							<img src="<?php bloginfo('template_directory'); ?>/images/gallery_Img1.jpg" alt="Gallery Img1">
						</div>
					</div>
					<div class="col-sm-4 col-xs-12 box">
						<div class="imgBox clearfix">
							<img src="<?php bloginfo('template_directory'); ?>/images/gallery_Img2.jpg" alt="Gallery Img2">
						</div>
					</div>
					<div class="col-sm-4 col-xs-12 box">
						<div class="imgBox clearfix">
							<img src="<?php bloginfo('template_directory'); ?>/images/gallery_Img3.jpg" alt="Gallery Img3">
						</div>
					</div>
				</div>
				<div class="bar clearfix">
					<div class="col-sm-4 col-xs-12 box">
						<div class="imgBox clearfix">
							<img src="<?php bloginfo('template_directory'); ?>/images/gallery_Img4.jpg" alt="Gallery Img4">
						</div>
					</div>
					<div class="col-sm-4 col-xs-12 box">
						<div class="imgBox clearfix">
							<img src="<?php bloginfo('template_directory'); ?>/images/gallery_Img5.jpg" alt="Gallery Img5">
						</div>
					</div>
					<div class="col-sm-4 col-xs-12 box">
						<div class="imgBox clearfix">
							<img src="<?php bloginfo('template_directory'); ?>/images/gallery_Img6.jpg" alt="Gallery Img6">
						</div>
					</div>
				</div>-->
				
			</div>
			
		</div>
	</section>
	<?php endwhile; else : ?>
	<p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; wp_reset_query(); ?>
<?php get_footer(); ?>