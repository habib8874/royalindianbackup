<?php get_header(); ?>
<section class="banner clearfix">
		<div id="banner" class="bannerSection clearfix">
			<div id="myCarousel" class="carousel slide">
				<!-- Indicators -->
				<ol class="carousel-indicators">
					<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
					<li data-target="#myCarousel" data-slide-to="1"></li>
					<li data-target="#myCarousel" data-slide-to="2"></li>
					<li data-target="#myCarousel" data-slide-to="3"></li>
					<li data-target="#myCarousel" data-slide-to="4"></li>
				</ol>

				<!-- Wrapper for Slides -->
				<div class="carousel-inner">

					<div class="item active">
						<div class="mask"></div>
						<div class="fill" style="background-image: url(<?php bloginfo('template_directory'); ?>/images/banner1.jpg)"></div>
					</div>
					<div class="item">
						<div class="mask"></div>
						<div class="fill" style="background-image: url(<?php bloginfo('template_directory'); ?>/images/banner2.jpg)"></div>
					</div>
					<div class="item">
						<div class="mask"></div>
						<div class="fill" style="background-image: url(<?php bloginfo('template_directory'); ?>/images/banner3.jpg)"></div>
					</div>
					<div class="item">
						<div class="mask"></div>
						<div class="fill" style="background-image: url(<?php bloginfo('template_directory'); ?>/images/banner4.jpg)"></div>
					</div>
					<div class="item">
						<div class="mask"></div>
						<div class="fill" style="background-image: url(<?php bloginfo('template_directory'); ?>/images/banner5.jpg)"></div>
					</div>
				</div>

				<!-- Controls -->
				<a class="left carousel-control" href="#myCarousel" data-slide="prev">
					<div class="icon">
						<span class="icon-prev"></span>
					</div>
				</a>
				<a class="right carousel-control" href="#myCarousel" data-slide="next">
					<div class="icon">
						<span class="icon-next"></span>
					</div>
				</a>
			</div>
		</div>
	</section>
	
	<section class="homeAboutSection clearfix">
		<div class="container">
			<span class="smallHeading">Welcome to</span>
			<h2 class="largeHeading">Royal Indian</h2>
			<div class="row content clearfix">
				<div class="col-sm-6 col-xs-12 left">
					<p>As the quote says, we are also sure that many of you know a lot and most of you know at least something about the Indian Cuisine. In fact, the different families of Indian cuisine are recognized by their sophisticated and subtle use of many spices and herbs. Each family of this cuisine is known by a wide assortment of dishes and cooking techniques. These spices, assortments, and cooking techniques are just perfect to enhance the flavor of a dish and create unique flavors and aromas.</p>
				</div>
				<div class="col-sm-6 col-xs-12 right">
					<p>Royale Indian Restaurant is the one which is serving selected and typical Indian cuisine with its real taste in Hamilton. Situated at the heart of downtown Hamilton, it is serving its wide range of food varieties - from mildly spicy to fiery hot, vegetarian and non-vegetarian, soups, naan bread, cheese or “paneer”, lentils, spinach, chicken - fish - lamb, mushrooms, samosa, and Dosa. Just give a look at our menu on this web page for further details of our mouthwatering food items.</p>
				</div>
			</div>
		</div>
	</section>
	
	<section class="testiSection clearfix">
		<div class="mask"></div>
		<!--<div class="container">
			
			
			<div>
				<div class="mask"></div>
				
					<i class="icon icon-arrow-left"></i>
					<i class="icon icon-arrow-right"></i>
				</div>
				<div class="testi clearfix">
				
					<p>"It was a fantastic evening in your superb restaurant which was enjoyed by everyone who was fortunate to be there" </p>
					
					<span>Julie Deowater</span>
					
				</div>
			</div>			
		</div>-->
		
		<div class="container">
			
			<span class="smallHeading">What</span>
			<h2 class="largeHeading">People Say About us</h2>
			
        <div class="testiBlock clearfix">
			
			<div class="carousel slide" data-ride="carousel" id="quote-carousel">
				<!-- Carousel Buttons Next/Prev -->
				<div class="navigation clearfix">
					 <a data-slide="prev" href="#quote-carousel" class="left carousel-control"><i class="icon icon-arrow-left"></i></a>
                    <a data-slide="next" href="#quote-carousel" class="right carousel-control"><i class="icon icon-arrow-right"></i></a>
				</div>
                   
                    <!-- Carousel Slides / Quotes -->
                    <div class="carousel-inner text-center">
                        <!-- Quote 1 -->
                        <div class="item active">
                            <div class="testi clearfix">
				
							<p>"It was a fantastic evening in your superb restaurant which was enjoyed by everyone who was fortunate to be there" </p>

							<span>Julie Deowater</span>
					
						</div>
                        </div>
                        <!-- Quote 2 -->
                        <div class="item">
                            <div class="testi clearfix">
				
							<p>"It was a fantastic evening in your superb restaurant which was enjoyed by everyone who was fortunate to be there" </p>

							<span>John Deowater</span>
					
						</div>
                        </div>
                        <!-- Quote 3 -->
                        <div class="item">
                            <div class="testi clearfix">
				
							<p>"It was a fantastic evening in your superb restaurant which was enjoyed by everyone who was fortunate to be there" </p>

							<span>Smith Deowater</span>
					
						</div>
                        </div>
                    </div>

                    
                </div>
			
        </div>
			
    	</div>
		
	</section>
	
	<section class="homeGallerySection clearfix">
		<div class="container">
			<span class="smallHeading">Our</span>
			<h2 class="largeHeading">Gallery</h2>
			
			<div class="galleryListBlock clearfix">
			
				<div class="bar clearfix">
					<div class="col-sm-4 col-xs-12 box">
						<div class="imgBox clearfix">
							<img src="<?php bloginfo('template_directory'); ?>/images/gallery_Img1.jpg" alt="Gallery Img1">
						</div>
					</div>
					<div class="col-sm-4 col-xs-12 box">
						<div class="imgBox clearfix">
							<img src="<?php bloginfo('template_directory'); ?>/images/gallery_Img2.jpg" alt="Gallery Img2">
						</div>
					</div>
					<div class="col-sm-4 col-xs-12 box">
						<div class="imgBox clearfix">
							<img src="<?php bloginfo('template_directory'); ?>/images/gallery_Img3.jpg" alt="Gallery Img3">
						</div>
					</div>
				</div>
				<div class="bar clearfix">
					<div class="col-sm-4 col-xs-12 box">
						<div class="imgBox clearfix">
							<img src="<?php bloginfo('template_directory'); ?>/images/gallery_Img4.jpg" alt="Gallery Img4">
						</div>
					</div>
					<div class="col-sm-4 col-xs-12 box">
						<div class="imgBox clearfix">
							<img src="<?php bloginfo('template_directory'); ?>/images/gallery_Img5.jpg" alt="Gallery Img5">
						</div>
					</div>
					<div class="col-sm-4 col-xs-12 box">
						<div class="imgBox clearfix">
							<img src="<?php bloginfo('template_directory'); ?>/images/gallery_Img6.jpg" alt="Gallery Img6">
						</div>
					</div>
				</div>
				
			</div>
			
		</div>
	</section>
<?php get_footer(); ?>