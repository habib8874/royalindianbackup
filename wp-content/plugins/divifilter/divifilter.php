<?php
/*
  Plugin Name:        Divi Filter Premium
  Plugin URI:         https://danielvoelk.de/en/divi-filter
  Description:        A plugin to filter every module in the Divi theme.
  Version:            3.1.0
  Author:             Daniel Völk
  Author URI:         https://danielvoelk.de/
  License:            GPL2
  License URI:        https://www.gnu.org/licenses/gpl-2.0.html
  
  Divi Filter is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 2 of the License, or
  any later version.
  
  Divi Filter is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with Divi Filter. If not, see https://www.gnu.org/licenses/gpl-2.0.html.
  */

  $server_output = "";

  /** Our plugin class */
  class DiviFilterPremium {
    public function __construct() {
      /** Step 1 (hook). */
      add_action( 'admin_menu', [ $this, 'my_plugin_menu' ] );
  
      /** Setup fields. */
      add_action( 'admin_init', [ $this, 'my_plugin_fields' ] );

      /** add filter files */
      add_action( 'wp_enqueue_scripts', [ $this, 'divifilter_add_files' ] );  

      /** add Upgrade link */
      add_filter( 'plugin_action_links_' . plugin_basename(__FILE__), [ $this, 'filter_action_links' ] );

      /** add Documentation link */
      add_filter( 'plugin_row_meta', [ $this, 'add_documentation_link' ], 10, 2 );
 
    }
  
    public function my_plugin_fields() {
      /** Check permissions. */
      if ( ! current_user_can( 'manage_options' ) )  {
        return;
      }
    
      add_settings_section( 'divi_field_section', 'Divi Filter', false, 'divi-filter-menu' );
    
      add_settings_field( 'key_input', 'Key', [ $this, 'field_callback' ], 'divi-filter-menu', 'divi_field_section' );
    
      register_setting( 'divi-filter-menu', 'key_input' );
    }
    
    public function field_callback() {
      echo '<input type="password" name="key_input" value="' . esc_attr( get_option( 'key_input' ) ) . '" /><br>';
      // echo get_option( 'key_input' );
      if((get_option('key_input') !== null) || (get_option('key_input') !== "")){ 

        $key_input = get_option('key_input');
        $domain_input = $_SERVER['SERVER_NAME'];

        add_option('keyValid', false);

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL,"https://danielvoelk.de/df-files/license/license-update-domain.php?key=$key_input&domain=$domain_input");
        curl_setopt($ch, CURLOPT_POST, 1);

        // Receive server response ...      COMMENT OUT LATER
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        global $server_output;
        $server_output = curl_exec($ch);
        curl_close ($ch);
         

        if (strpos($server_output, 'PREMIUM') !== false) {
          update_option('keyValid', true);
        }
        else {
          update_option('keyValid', false);
        }

      } else{
        echo "Key not set"; 
      }
    }
  
    /** Step 2 (add item). */
    public function my_plugin_menu() {
      $page_title = 'Divi Filter Options';
      $menu_title = 'Divi Filter';
      $capability = 'manage_options'; // Only users that can manage options can access this menu item.
      $menu_slug  = 'divi-filter'; // unique identifier.
      $callback   = [ $this, 'my_plugin_options' ];
      add_options_page( $page_title, $menu_title, $capability, $menu_slug, $callback );
    }

    /** Step 3 (page html). */
    public function my_plugin_options() { ?>
      <div class="wrap">
        <!-- <h2>Your plugin peag</h2> -->
        <form method="post" action="options.php">
          <?php settings_fields( 'divi-filter-menu' ); ?>
          <?php do_settings_sections( 'divi-filter-menu' ); ?>
          <?php submit_button(); ?>
          <?php global $server_output;
          echo "Status: " . $server_output; ?>
        </form>
      </div>
      <?php
    }

    public function filter_action_links( $links ) {

      if( !get_option('keyValid') ) {       // Check to see if premium version already installed
        // echo get_option('keyValid');
        $links['upgrade'] = '<a style="font-weight: bold;" href="https://danielvoelk.de/en/divi-filter/#upgrade" target="_blank">Go Premium</a>';;
      }
      // $links['documentation'] = '<a href="https://danielvoelk.de/en/divi-filter/#faq" target="_blank">Documentation</a>';
      return $links;
     }
    
    public function divifilter_add_files() {

      // echo "<script>alert($key_input);</script>";
      echo "<script type='text/javascript'><!--
      var key_input = '".get_option('key_input')."';
      </script>";

      wp_register_script('df-script', plugins_url('df-script.js', __FILE__), array('jquery'),'3.1.0', true);
      wp_enqueue_script('df-script');
    
      wp_register_style('df-style', plugins_url('df-style.css', __FILE__), array(), '3.1.0');
      wp_enqueue_style('df-style');
    
    }

    public function add_documentation_link( $links, $file ) {    
      if ( plugin_basename( __FILE__ ) == $file ) {
        $row_meta = array(
          'docs'    => '<a href="https://danielvoelk.de/en/divi-filter/#faq" target="_blank">Documentation</a>'
        );

        return array_merge( $links, $row_meta );
      }
      return (array) $links;
    }

}

new DiviFilterPremium();

// auto update
require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://danielvoelk.de/df-files/version.json',
	__FILE__ //Full path to the main plugin file or functions.php.
	// 'unique-plugin-or-theme-slug'
);

    
?>